﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using GPUInstancer;
using UnityStandardAssets.ImageEffects;
using DG.Tweening;
using MoreMountains.NiceVibrations;
using PaintIn3D;
using UnityEngine.EventSystems;
using PaintIn3D.Examples;
using System.Linq;
using JetBrains.Annotations;
using GameAnalyticsSDK;

public class Controller : MonoBehaviour
{
    public static Controller instance;
    public int speed;
    public bool isDrag = false;
    int ballCount;
    public GameObject winPanel;
    public Slider levelProgress;
    public Text currentLevelText;
    public Text totalMoneyText;
    public Text progressText;
    public GameObject loadingBar;
    public static int limitFall = 5;
    public static int limitParticle = 5;
    public int currentLevel;
    int money;
    int totalMoney;
    bool isStartGame = false;
    public GameObject ground;
    public GameObject plusVarPrefab;
    public Canvas canvas;
    bool isVibrate = true;
    public static int colorCode;
    public LayerMask dragMask;
    Coroutine dropRoutine;
    public Transform cutter;
    public Transform crayon;
    public int playMode = 99;
    public int colorID;
    public List<Color> listBG = new List<Color>();
    public List<GameObject> listDrop = new List<GameObject>();
    public List<GameObject> listLevel = new List<GameObject>();

    public List<Sprite> listSamples = new List<Sprite>();
    [System.Serializable]
    public class LayersUI
    {
        public List<Sprite> listLayersUI = new List<Sprite>();
    }
    public List<LayersUI> listLayersUI = new List<LayersUI>();

    [System.Serializable]
    public class Colors
    {
        public List<Color> listColors = new List<Color>();
    }
    public List<Colors> listColors = new List<Colors>();

    public Image sample;
    public GameObject colorZone;
    public GameObject layerZone;
    public GameObject offerZone;
    public GameObject buttonPaint;
    public GameObject buttonSell;
    public GameObject bgButtons;
    public Slider colorBar;
    public static Color currentColor;
    public List<Color> layerBG = new List<Color>();
    public float height = 0.5f;
    public GameObject currentLayer;
    public GameObject mapReader;
    public GameObject GPUInstance;
    public float scoreOffer = 0;
    public int numOfLayers = 0;
    public GameObject confetti;
    public List<Sprite> listAvatar = new List<Sprite>();
    public Image avatar;
    public Text moneyOffer;
    public Text offerIndex;
    public int offerIndexCount = 1;
    public List<Color> listColorPick = new List<Color>();
    public List<GameObject> tutorialSteps = new List<GameObject>();
    public List<GameObject> listTools = new List<GameObject>();
    public List<GameObject> listToolsObject = new List<GameObject>();
    int toolID = 0;
    bool isTutorial = false;
    int tutorialStep = 0;
    public GameObject handPaint;
    public Image logo;
    public GameObject coinAnimation;
    public GameObject nextOfferButton;
    public GameObject acceptButton;
    public Transform frame;
    public List<int> listCheckPaint = new List<int>();
    int currentLayerID = 0;

    //public void Awake()
    //{
    //    GameAnalytics.Initialize();
    //}

    private void Start()
    {
        Application.targetFrameRate = 60;
        playMode = 99;
        Input.multiTouchEnabled = false;
        instance = this;
        currentLevel = PlayerPrefs.GetInt("currentLevel");
        totalMoney = PlayerPrefs.GetInt("totalMoney");
        totalMoneyText.text = totalMoney.ToString();
        currentLevelText.text = "LEVEL " + currentLevel.ToString();
        //GameAnalyticsManager.Instance.Log_StartLevel();
        for (int i = 0; i < listTools.Count; i++)
        {
            if(i > 0)
            {
                var checkUnlock = PlayerPrefs.GetInt("Tool" + i.ToString());
                if(checkUnlock == 1)
                {
                    listTools[i].transform.GetChild(0).gameObject.SetActive(false);
                }
            }
        }
        limitFall = 10;
        limitParticle = 5;
        logo.DOFade(0, 2);
        Destroy(logo.gameObject, 2);
        var randomColor = Random.Range(0, 3);
        if(randomColor == 3)
        {
            randomColor = 2;
        }
        ground.GetComponent<MeshRenderer>().material.color = listBG[randomColor];

        if(currentLevel == 0)
        {
            isTutorial = true;
            tutorialSteps[tutorialStep].SetActive(true);
        }

        levelProgress.maxValue = /*listLevel[currentLevel].transform.childCount*/100;
        levelProgress.value = 0;
        cutter.GetComponent<Animator>().speed = 0;
        progressText.text = levelProgress.value.ToString() /*+ "/" + levelProgress.maxValue.ToString()*/;

        //Data loading
        sample.sprite = listSamples[currentLevel];

        //var colorPrefab = colorZone.transform.GetChild(0).gameObject;
        //colorPrefab.GetComponent<Image>().color = listColors[currentLevel].listColors[0];
        //colorPrefab.name = "0";
        //for (int i = 1; i < listColors[currentLevel].listColors.Count; i++)
        //{
        //    var colorClone = Instantiate(colorPrefab, colorZone.transform);
        //    colorClone.GetComponent<Image>().color = listColors[currentLevel].listColors[i];
        //    colorClone.name = i.ToString();
        //}

        var layerPrefab = layerZone.transform.GetChild(0).gameObject;
        //var shuffledList = listLayersUI[currentLevel].listLayersUI.OrderBy(x => Random.value).ToList();
        //layerPrefab.GetComponent<Image>().color = layerBG[0];
        layerPrefab.transform.GetChild(0).GetComponent<Image>().sprite = listLayersUI[currentLevel].listLayersUI[0];
        layerPrefab.name = "0";
        listCheckPaint.Add(0);
        for (int i = 1; i < listLayersUI[currentLevel].listLayersUI.Count; i++)
        {
            var layerClone = Instantiate(layerPrefab, layerZone.transform);
            //layerClone.GetComponent<Image>().color = layerBG[i];
            layerClone.transform.GetChild(0).GetComponent<Image>().sprite = listLayersUI[currentLevel].listLayersUI[i];
            layerClone.name = i.ToString();
            listCheckPaint.Add(0);
        }
        numOfLayers = layerZone.transform.childCount;
        //UnityAdsManager.instance.ShowRewardedVideo();
    }

    private void FixedUpdate()
    {
        if (isStartGame)
        {
            if(playMode == 0)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    //UnityAdsManager.instance.ShowRewardedVideo();
                    isDrag = true;
                }

                if (Input.GetMouseButton(0))
                {
                    OnMousePlace();
                }

                if (Input.GetMouseButtonUp(0))
                {
                    isDrag = false;
                }
            }
            else if (playMode == 1)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hit;

                    if (Physics.Raycast(ray, out hit, 1000, dragMask))
                    {
                        if (hit.transform != null)
                        {
                            isDrag = true;
                        }
                    }

                    if (dropRoutine == null && isDrag)
                        dropRoutine = StartCoroutine(Drop());
                }

                if (Input.GetMouseButton(0))
                {
                    OnMouseDrop();
                }

                if (Input.GetMouseButtonUp(0))
                {
                    isDrag = false;
                    if (dropRoutine != null)
                    {
                        StopCoroutine(dropRoutine);
                        dropRoutine = null;
                        cutter.GetComponent<Animator>().speed = 0;
                    }
                }
            }
            else if(playMode == 2)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    isDrag = true;
                }

                if (Input.GetMouseButton(0))
                {
                    OnMousePaint();
                }

                if (Input.GetMouseButtonUp(0))
                {
                    isDrag = false;
                    transform.position = new Vector3(transform.position.x, 10, transform.position.z);
                }
            }
        }
        if(Input.GetKeyDown(KeyCode.D))
        {
            PlayerPrefs.DeleteAll();
        }
    }

    void OnMousePlace()
    {
        if (isTutorial && tutorialStep == 1)
        {
            tutorialSteps[tutorialStep].SetActive(false);
            tutorialStep++;
            tutorialSteps[tutorialStep].SetActive(true);
        }
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 1000, dragMask))
        {
            if (playMode == 0)
            {
                transform.position = new Vector3(hit.point.x, height, hit.point.z);
            }
        }
    }

    void OnMouseDrop()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 1000, dragMask))
        {
            transform.position = new Vector3(hit.point.x, 10, hit.point.z);
        }
    }

    void OnMousePaint()
    {
        if (isTutorial && tutorialStep == 5)
        {
            tutorialSteps[tutorialStep].SetActive(false);
            tutorialStep++;
            tutorialSteps[tutorialStep].SetActive(true);
        }
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 1000, dragMask))
        {
            transform.position = new Vector3(hit.point.x, height, hit.point.z);
            PlusEffect();
            if (listCheckPaint[currentLayerID] == 0)
            {
                listCheckPaint[currentLayerID] = 1;
                int checkCount = 0;
                foreach (var item in listCheckPaint)
                {
                    if (item == 1)
                    {
                        checkCount++;
                    }
                }
                if (checkCount == listCheckPaint.Count)
                    buttonSell.SetActive(true);
            }
        }
    }

    IEnumerator Drop()
    {
        loadingBar.SetActive(true);
        loadingBar.transform.GetChild(0).GetComponent<Slider>().value = 0;
        while (loadingBar.transform.GetChild(0).GetComponent<Slider>().value < 1)
        {
            loadingBar.transform.GetChild(0).GetComponent<Slider>().value += 0.05f;
            yield return null;
        }
        loadingBar.SetActive(false);
        while (ReadPositionFileText.instance.totalPixel > 1)
        {
            RaycastHit hit;
            int count = 0;
            if (Physics.BoxCast(transform.position, new Vector3(transform.localScale.x * 0.5f, transform.localScale.y * 0.5f, transform.localScale.z * 0.5f), Vector3.down, out hit, Quaternion.Euler(0, 0, 0), 50))
            {
                if (hit.transform.CompareTag("Ball"))
                {
                    count++;
                }
                if (count == 0)
                {
                    try
                    {
                        //for (int i = 0; i < 5; i++)
                        //{
                            cutter.GetComponent<Animator>().speed = 5;
                            ReadPositionFileText.instance.totalPixel--;
                            AddRemoveInstances.instance.instancesList[ReadPositionFileText.instance.totalPixel].GetComponent<Ball>().Fall();
                        //}
                    }
                    catch { }
                }
            }
            yield return new WaitForSeconds(0.01f);
            PlusEffect();
            if (isTutorial && tutorialStep == 3)
            {
                tutorialSteps[tutorialStep].SetActive(false);
                tutorialStep++;
                tutorialSteps[tutorialStep].SetActive(true);
            }
            //cutter.GetComponent<Animator>().speed = 0;
        }

    }

    public void ChangeColorButton()
    {
        PlusEffect();
        if(currentLayer == null)
        {
            return;
        }
        if (listDrop.Count > 0)
        {
            foreach (var item in listDrop)
            {
                AddRemoveInstances.instance.RemoveInstances(item.gameObject.GetComponent<GPUInstancerPrefab>());
                Destroy(item.gameObject, 0.1f);
            }
            listDrop.Clear();
        }
        colorBar.gameObject.SetActive(true);
        colorBar.value = 1;
        if (isTutorial && tutorialStep == 2)
        {
            tutorialSteps[tutorialStep].SetActive(false);
            tutorialStep++;
            tutorialSteps[tutorialStep].SetActive(true);
        }
        if (playMode != 1)
        {
            playMode = 1;
            GPUInstance.SetActive(false);
            crayon.gameObject.SetActive(true);
            cutter.gameObject.SetActive(true);
            handPaint.SetActive(false);
            mapReader.SetActive(true);
            //colorID = int.Parse(EventSystem.current.currentSelectedGameObject.name);
            currentColor = EventSystem.current.currentSelectedGameObject.GetComponent<Image>().color;
            crayon.GetComponent<MeshRenderer>().material.color = currentColor;
            colorBar.transform.GetChild(0).transform.GetChild(0).GetComponent<Image>().material.SetColor("_ColorMid", currentColor);
            //Debug.Log(colorID);
            foreach (Transform child in crayon.transform)
            {
                child.GetComponent<Ball>().ChangeColor(currentColor);
            }
            AddRemoveInstances.instance.RefreshColor();
            //Destroy(EventSystem.current.currentSelectedGameObject, 0.2f);
            var currentChildCount = transform.childCount;
            if(currentChildCount > 3)
                transform.GetChild(3).parent = null;
            transform.position = new Vector3(0, 10, 0);
            if(currentLayer != null)
                currentLayer.transform.GetChild(0).GetComponent<MeshRenderer>().sharedMaterial.color = new Color32(255, 255, 255, 255);
        }
        else
        {
            //GPUInstance.SetActive(false);
            crayon.gameObject.SetActive(true);
            cutter.gameObject.SetActive(true);
            handPaint.SetActive(false);
            //mapReader.SetActive(true);
            //colorID = int.Parse(EventSystem.current.currentSelectedGameObject.name);
            currentColor = EventSystem.current.currentSelectedGameObject.GetComponent<Image>().color;
            crayon.GetComponent<MeshRenderer>().material.color = currentColor;
            colorBar.transform.GetChild(0).transform.GetChild(0).GetComponent<Image>().material.SetColor("_ColorMid", currentColor);
            //Debug.Log(colorID);
            foreach (Transform child in crayon.transform)
            {
                child.GetComponent<Ball>().ChangeColor(currentColor);
            }
            AddRemoveInstances.instance.RefreshColor();
            //Destroy(EventSystem.current.currentSelectedGameObject, 0.2f);
            if (currentLayer != null)
                currentLayer.transform.GetChild(0).GetComponent<MeshRenderer>().sharedMaterial.color = new Color32(255, 255, 255, 255);
        }
    }
    
    public void OnChangeColorBar()
    {
        if (playMode != 1)
        {
            crayon.GetComponent<MeshRenderer>().material.color = currentColor * colorBar.value;
            //Debug.Log(colorID);
            foreach (Transform child in crayon.transform)
            {
                child.GetComponent<Ball>().ChangeColor(currentColor * colorBar.value);
            }
            AddRemoveInstances.instance.RefreshColor();
            //Destroy(EventSystem.current.currentSelectedGameObject, 0.2f);
            var currentChildCount = transform.childCount;
            if (currentChildCount > 3)
                transform.GetChild(3).parent = null;
            transform.position = new Vector3(0, 10, 0);
            if (currentLayer != null)
                currentLayer.transform.GetChild(0).GetComponent<MeshRenderer>().sharedMaterial.color = new Color32(255, 255, 255, 255);
        }
        else
        {
            crayon.GetComponent<MeshRenderer>().material.color = currentColor * colorBar.value;
            //Debug.Log(colorID);
            foreach (Transform child in crayon.transform)
            {
                child.GetComponent<Ball>().ChangeColor(currentColor * colorBar.value);
            }
            AddRemoveInstances.instance.RefreshColor();
            //Destroy(EventSystem.current.currentSelectedGameObject, 0.2f);
            if (currentLayer != null)
                currentLayer.transform.GetChild(0).GetComponent<MeshRenderer>().sharedMaterial.color = new Color32(255, 255, 255, 255);
        }
    }

    GameObject buttonLayer;
    int layerCount = 0;
    public void ChangeLayerButton()
    {
        PlusEffect();
        if (isTutorial && tutorialStep == 0)
        {
            tutorialSteps[tutorialStep].SetActive(false);
            tutorialStep++;
            tutorialSteps[tutorialStep].SetActive(true);
        }
        playMode = 0;
        //levelProgress.value++;
        //progressText.text = levelProgress.value.ToString() + "/" + levelProgress.maxValue.ToString();
        if (listDrop.Count > 0)
        {
            foreach (var item in listDrop)
            {
                AddRemoveInstances.instance.RemoveInstances(item.gameObject.GetComponent<GPUInstancerPrefab>());
                item.transform.DOMoveY(100, 0);
                item.gameObject.GetComponent<P3dPaintSphere>().enabled = false;
                Destroy(item.gameObject, 0.1f);
            }
            listDrop.Clear();
        }
        if (!isStartGame)
        {
            isStartGame = true;
        }
        else if (currentLayer != null)
        {
            //height += 0.25f;
            //currentLayer.transform.DOMoveY(height / 2, 0);
            GameObject insideLayer = currentLayer.transform.GetChild(0).gameObject;
            var total = insideLayer.GetComponent<P3dChannelCounter>().Total;
            if (total == 0)
                total = 1;
            var current = total - insideLayer.GetComponent<P3dChannelCounter>().CountR;
            if (current < 0)
                current = 0;
            scoreOffer += (current * 100 / total);
            levelProgress.value += scoreOffer;
            progressText.text = levelProgress.value.ToString();
            //Debug.LogError(total + " " + current + " " + scoreOffer);
            layerCount++;
            insideLayer.GetComponent<MeshRenderer>().sharedMaterial.renderQueue = 2000 + layerCount;
            insideLayer.transform.parent = null;
            insideLayer.GetComponent<P3dPaintableTexture>().enabled = false;
            insideLayer.GetComponent<P3dPaintable>().enabled = false;
            insideLayer.transform.DOMoveY(0, 0);
            currentLayer.transform.DOMoveX(currentLayer.transform.position.x - 25, 0.5f);
            var destroy = currentLayer;
            currentLayer = null;
            Destroy(destroy, 0.5f);
        }
        if (buttonLayer != null)
            buttonLayer.transform.DOScale(Vector3.one, 0.35f);
        if (EventSystem.current.currentSelectedGameObject != buttonLayer)
        {
            crayon.gameObject.SetActive(false);
            cutter.gameObject.SetActive(false);
            handPaint.SetActive(false);
            transform.position = new Vector3(0, height, 0);
            buttonLayer = EventSystem.current.currentSelectedGameObject;
            buttonLayer.transform.DOScale(Vector3.one * 1.2f, 0.25f);
            int id = int.Parse(EventSystem.current.currentSelectedGameObject.name);
            currentLayerID = id;
            var layerObjectPrefab = Instantiate(listLevel[currentLevel].transform.GetChild(id).gameObject);
            layerObjectPrefab.transform.transform.localScale = listLevel[currentLevel].transform.localScale;
            layerObjectPrefab.transform.transform.rotation = listLevel[currentLevel].transform.rotation;
            layerObjectPrefab.transform.DOMoveX(25, 0);
            layerObjectPrefab.transform.DOMoveX(0, 0.5f);
            layerObjectPrefab.transform.parent = transform;
            layerObjectPrefab.transform.DOLocalMove(Vector3.zero, 0.5f);
            StartCoroutine(delayNewLayerPaintable(layerObjectPrefab));
            StartCoroutine(delayNewLayerPaintable(layerObjectPrefab.transform.GetChild(0).gameObject));
            Color32 bgLayerColor = EventSystem.current.currentSelectedGameObject.GetComponent<Image>().color;
            //layerObjectPrefab.GetComponent<MeshRenderer>().sharedMaterial.color = new Color32(bgLayerColor.r, bgLayerColor.g, bgLayerColor.b, 150);
            layerObjectPrefab.transform.GetChild(0).GetComponent<MeshRenderer>().sharedMaterial.color = new Color32(255, 255, 255, 0);
            currentLayer = layerObjectPrefab;
            //Destroy(EventSystem.current.currentSelectedGameObject, 0.2f);
            if (isTutorial)
                transform.position = new Vector3(10, height, 0);
        }
        else
        {
            buttonLayer = null;
        }
    }

    IEnumerator delayNewLayerPaintable(GameObject target)
    {
        yield return new WaitForSeconds(0.5f);
        target.GetComponent<P3dPaintable>().enabled = true;
    }

    public void PaintButton()
    {
        PlusEffect();
        if(currentLayer != null)
        {
            currentLayer.transform.parent = null;
        }
        listToolsObject[toolID].gameObject.SetActive(false);
        toolID = int.Parse((EventSystem.current.currentSelectedGameObject.name).ToString());
        var checkUnlock = PlayerPrefs.GetInt("Tool" + toolID.ToString());
        if (checkUnlock == 0 && toolID > 0)
        {
            if (checkUnlock == 0 && totalMoney >= 1000)
            {
                totalMoney -= 1000;
                PlayerPrefs.SetInt("totalMoney", totalMoney);
                listTools[toolID].transform.GetChild(0).gameObject.SetActive(false);
                PlayerPrefs.SetInt("Tool" + toolID.ToString(), 1);
            }
            else
            {
                return;
            }
        }

        listToolsObject[toolID].gameObject.SetActive(true);
        if (toolID > 0)
        {
            //AddRemoveInstances.instance.prefab.GetComponent<P3dPaintSphere>().Radius = 0.2f + 0.2f*toolID;
            foreach(var item in listDrop)
            {
                item.GetComponent<P3dPaintSphere>().Radius = 0.2f + 0.2f * toolID;
            }
        }
        else
        {
            //AddRemoveInstances.instance.prefab.GetComponent<P3dPaintSphere>().Radius = 0.2f;
            foreach (var item in listDrop)
            {
                item.GetComponent<P3dPaintSphere>().Radius = 0.2f + 0.2f * toolID;
            }
        }
        for (int i = 0; i < listTools.Count; i++)
        {
            if (i != toolID)
            {
                listTools[i].GetComponent<Image>().color = new Color32(255, 255, 255, 100);
            }
            else
            {
                listTools[i].GetComponent<Image>().color = new Color32(255, 255, 255, 255);
            }
        }

        if (isTutorial && tutorialStep == 4)
        {
            tutorialSteps[tutorialStep].SetActive(false);
            tutorialStep++;
            tutorialSteps[tutorialStep].SetActive(true);
        }
        playMode = 2;
        mapReader.SetActive(false);
        //foreach(var item in AddRemoveInstances.instance.instancesList)
        //{
        //    AddRemoveInstances.instance.RemoveInstances(item.GetComponent<GPUInstancerPrefab>());
        //    Destroy(item.gameObject, 1);
        //}
        //GPUInstance.SetActive(false);
        crayon.gameObject.SetActive(false);
        cutter.gameObject.SetActive(false);
        handPaint.SetActive(true);
        if (crayon.childCount > 0)
        {
            foreach (Transform child in crayon.transform)
            {
                AddRemoveInstances.instance.RemoveInstances(child.gameObject.GetComponent<GPUInstancerPrefab>());
                Destroy(child.gameObject, 0.5f);
            }
        }
    }

    public void SellButton()
    {
        PlusEffect();
        if (isTutorial && tutorialStep == 6)
        {
            tutorialSteps[tutorialStep].SetActive(false);
        }
        buttonSell.SetActive(false);
        layerZone.SetActive(false);
        colorZone.SetActive(false);
        buttonPaint.SetActive(false);
        bgButtons.SetActive(false);
        colorBar.gameObject.SetActive(false);
        frame.transform.DOLocalMoveY(0.5f, 1);
        if (listDrop.Count > 0)
        {
            foreach (var item in listDrop)
            {
                AddRemoveInstances.instance.RemoveInstances(item.gameObject.GetComponent<GPUInstancerPrefab>());
                Destroy(item.gameObject, 0.1f);
            }
            listDrop.Clear();
        }
        if (currentLayer != null)
        {
            //height += 0.25f;
            //currentLayer.transform.DOMoveY(height / 2, 0);
            GameObject insideLayer = currentLayer.transform.GetChild(0).gameObject;
            var total = insideLayer.GetComponent<P3dChannelCounter>().Total;
            var current = total - insideLayer.GetComponent<P3dChannelCounter>().CountR;
            scoreOffer += (current * 100 / total);
            levelProgress.value += scoreOffer;
            progressText.text = levelProgress.value.ToString();
            //Debug.LogError(total + " " + current + " " + scoreOffer);
            layerCount++;
            insideLayer.GetComponent<MeshRenderer>().sharedMaterial.renderQueue = 2000 + layerCount;
            insideLayer.transform.parent = null;
            insideLayer.GetComponent<P3dPaintableTexture>().enabled = false;
            insideLayer.GetComponent<P3dPaintable>().enabled = false;
            insideLayer.transform.DOMoveY(0, 0);
            currentLayer.transform.DOMoveX(currentLayer.transform.position.x - 25, 0.5f);
            var destroy = currentLayer;
            currentLayer = null;
            Destroy(destroy, 0.5f);
        }

        crayon.gameObject.SetActive(false);
        cutter.gameObject.SetActive(false);
        handPaint.SetActive(false);
        transform.position = new Vector3(0, height, 0);

        winPanel.SetActive(true);
        MMVibrationManager.Vibrate();
        confetti.SetActive(true);
        scoreOffer /= numOfLayers;
        for (int i = 0; i < 3; i++)
        {
            offerZone = winPanel.transform.GetChild(0).transform.GetChild(i).gameObject;
            offerZone.transform.localScale = Vector3.zero;
            offerZone.SetActive(true);
            offerZone.transform.DOScale(0.8f, 0.25f);
            money = (int)scoreOffer * 5;
            money += Random.Range(1, 30);
            offerZone.name = money.ToString();
            moneyOffer = offerZone.transform.GetChild(2).transform.GetChild(0).transform.GetChild(1).GetComponent<Text>();
            moneyOffer.text = money.ToString();
            int getAvatar = Random.Range(0, listAvatar.Count);
            avatar = offerZone.transform.GetChild(1).GetComponent<Image>();
            avatar.sprite = listAvatar[getAvatar];
            listAvatar.RemoveAt(getAvatar);
            //offerIndex.text = offerIndexCount.ToString() + "/3";
        }

        sample.transform.parent.transform.parent.gameObject.SetActive(false);
        buttonSell.SetActive(false);
        levelProgress.gameObject.SetActive(false);
        Camera.main.transform.DOMoveZ(-6.7f, 0.5f);
        Camera.main.transform.DOMoveY(35f, 0.5f);
    }

    //public void NextOfferButton()
    //{
    //    var offerIndexBubble = offerIndex.transform.parent;
    //    var moneyOfferBubble = moneyOffer.transform.parent.transform.parent;
    //    offerIndexBubble.transform.DOScale(0, 0.25f).SetLoops(2, LoopType.Yoyo);
    //    moneyOfferBubble.transform.DOScale(0, 0.25f).SetLoops(2, LoopType.Yoyo);
    //    if (offerIndexCount <= 2)
    //    {
    //        int minimumOffer = 10;
    //        money = (int)scoreOffer * 10;
    //        if (money <= 10)
    //        {
    //            minimumOffer = 10;
    //        }
    //        else
    //        {
    //            minimumOffer = (money - 10) * (-1);
    //        }
    //        money += Random.Range(minimumOffer, 50);
    //        money /= 10;
    //        moneyOffer.text = money.ToString();
    //        int getAvatar = Random.Range(0, listAvatar.Count);
    //        avatar.sprite = listAvatar[getAvatar];
    //        listAvatar.RemoveAt(getAvatar);
    //        offerIndexCount++;
    //        offerIndex.text = offerIndexCount.ToString() + "/3";
    //    }
    //    if(offerIndexCount > 1)
    //    {
    //        nextOfferButton.GetComponent<Animator>().enabled = false;
    //        nextOfferButton.SetActive(false);
    //        acceptButton.GetComponent<Animator>().enabled = true;
    //    }
    //}

    public void PlusEffect()
    {
        if (!UnityEngine.iOS.Device.generation.ToString().Contains("5") && isVibrate)
        {
            isVibrate = false;
            MMVibrationManager.Haptic(HapticTypes.LightImpact);
            StartCoroutine(delayVibrate());
            Debug.Log("Vibrate");
        }
    }

    IEnumerator delayVibrate()
    {
        yield return new WaitForSeconds(0.2f);
        isVibrate = true;
    }

    public Vector3 worldToUISpace(Canvas parentCanvas, Vector3 worldPos)
    {
        //Convert the world for screen point so that it can be used with ScreenPointToLocalPointInRectangle function
        Vector3 screenPos = Camera.main.WorldToScreenPoint(worldPos);
        Vector2 movePos;

        //Convert the screenpoint to ui rectangle local point
        RectTransformUtility.ScreenPointToLocalPointInRectangle(parentCanvas.transform as RectTransform, screenPos, parentCanvas.worldCamera, out movePos);
        //Convert the local point to world point
        return parentCanvas.transform.TransformPoint(movePos);
    }

    public void LimitFallChange()
    {
        limitFall--;
        StartCoroutine(delayFall());
    }

    IEnumerator delayFall()
    {
        yield return new WaitForSeconds(0.001f);
        limitFall++;
    }

    public void LimitParticleChange()
    {
        limitParticle--;
        StartCoroutine(delayParticle());
    }

    IEnumerator delayParticle()
    {
        yield return new WaitForSeconds(0.3f);
        limitParticle++;
        Debug.Log(limitParticle);
    }

    public void NextMap()
    {
        isStartGame = false;
        GameObject currentButton = EventSystem.current.currentSelectedGameObject.transform.parent.gameObject;
        money = int.Parse((currentButton.name).ToString());
        totalMoney += money;
        currentLevel++;
        if(currentLevel >= listLevel.Count)
        {
            currentLevel = 0;
        }
        //while(currentLevel == 16 || currentLevel == 25 || currentLevel == 29 || currentLevel == 33 || currentLevel == 36 || currentLevel == 37 /*|| currentLevel == 40*/ || currentLevel == 41 || currentLevel == 44 || currentLevel == 50 || currentLevel == 51 || currentLevel == 54 || currentLevel == 55 /*|| currentLevel == 57*/ /*|| currentLevel == 58*/ /*|| currentLevel == 61*/ || currentLevel == 63)
        //{
        //    currentLevel++;
        //}
        PlayerPrefs.SetInt("totalMoney", totalMoney);
        PlayerPrefs.SetInt("currentLevel", currentLevel);
        coinAnimation = currentButton.transform.GetChild(2).transform.GetChild(0).transform.GetChild(0).gameObject;
        coinAnimation.SetActive(true);
        acceptButton = currentButton.transform.GetChild(4).gameObject;
        //acceptButton.GetComponent<Animator>().enabled = false;
        acceptButton.SetActive(false);
        StartCoroutine(delayLoadScene());
    }

    public InputField levelInput;
    public void OnChangeLevel()
    {
        var level = int.Parse(levelInput.text.ToString());
        PlayerPrefs.SetInt("currentLevel", level);
        StartCoroutine(delayLoadScene());
    }

    public void OnNext()
    {
        currentLevel++;
        if (currentLevel >= listLevel.Count)
        {
            currentLevel = 0;
        }
        PlayerPrefs.SetInt("currentLevel", currentLevel);
        StartCoroutine(delayLoadScene());
    }

    public void OnBack()
    {
        if (currentLevel > 0)
        {
            currentLevel--;
        }
        PlayerPrefs.SetInt("currentLevel", currentLevel);
        StartCoroutine(delayLoadScene());
    }

    public void Restart()
    {
        StartCoroutine(delayLoadScene());
    }

    IEnumerator delayLoadScene()
    {
        yield return new WaitForSeconds(1);
        totalMoneyText.text = totalMoney.ToString();
        totalMoneyText.transform.DOScale(1.1f, 0.5f).SetLoops(2, LoopType.Yoyo);
        //GameAnalyticsManager.Instance.Log_EndLevel();
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(0);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Ball") && playMode == 2 && transform.childCount < 10)
        {
            if (!other.GetComponent<Ball>().isDrag)
            {
                other.GetComponent<Ball>().isDrag = true;
                other.GetComponent<P3dHitNearby>().enabled = true;
                other.transform.parent = transform;
                AddRemoveInstances.instance.RemoveInstances(other.GetComponent<GPUInstancerPrefab>());
                listDrop.Remove(other.gameObject);
                //other.GetComponent<MeshRenderer>().enabled = false;
                Destroy(other.gameObject, 1);
            }
        }
    }

    [NaughtyAttributes.Button]
    public void SortList()
    {
        //listColors = listColors.OrderBy(o => o.listColors.Count).ToList();
        //listLayersUI = listLayersUI.OrderBy(o => o.listLayersUI.Count).ToList();
        //listLevel = listLevel.OrderBy(o => o.listLevel.Count).ToList();
        for (int i = 0; i < listLevel.Count - 1; i++)
        {
            if (i == 4 || i == 5 || i == 6 || i == 7 || i == 10 || i == 16 || i == 18 || i == 19)
            {
                listLevel[i] = null;
            }
        }
        listLevel.RemoveAll(item => item == null);

        for (int i = 0; i < listLayersUI.Count - 1; i++)
        {
            if (i == 4 || i == 5 || i == 6 || i == 7 || i == 10 || i == 16 || i == 18 || i == 19)
            {
                listLayersUI[i] = null;
            }
        }
        listLayersUI.RemoveAll(item => item == null);

        for (int i = 0; i < listSamples.Count - 1; i++)
        {
            if (i == 4 || i == 5 || i == 6 || i == 7 || i == 10 || i == 16 || i == 18 || i == 19)
            {
                listSamples[i] = null;
            }
        }
        listSamples.RemoveAll(item => item == null);
    }
}