using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MoreMountains.NiceVibrations;
using UnityEngine.EventSystems;
using DG.Tweening;
using GPUInstancer;
using PaintIn3D;

public class Ball : MonoBehaviour
{
    public Color ballColor;
    public GameObject ballParticle;
    bool isFall = false;
    public bool isDrag = false;

    private void Start()
    {

    }

    public void Fall()
    {
        if (Controller.instance.isDrag && !isFall)
        {
            isFall = true;
            Controller.instance.LimitFallChange();
            transform.parent = null;
            transform.DOLocalMove(new Vector3(transform.position.x + Random.Range(-1f, 1f), Controller.instance.height, transform.position.z + Random.Range(-1f, 1f)), 0.1f);
            Controller.instance.listDrop.Add(gameObject);
            AddRemoveInstances.instance.instancesList.Remove(GetComponent<GPUInstancerPrefab>());
        }
    }

    public void ChangeColor(Color currentColor)
    {
        if(currentColor.a < 0.5f)
        {
            currentColor.a = 0.5f;
        }
        GetComponent<MeshRenderer>().material.color = currentColor;/*new Color32((byte)currentColor.r, (byte)currentColor.g, (byte)currentColor.b, 255);*/
        GetComponent<P3dPaintSphere>().Color = currentColor;/*new Color32((byte)currentColor.r, (byte)currentColor.g, (byte)currentColor.b, 255);*/
    }
}
