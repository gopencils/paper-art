using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;
using GPUInstancer;

public class ReadPositionFileText : MonoBehaviour
{
    public static ReadPositionFileText instance;
    public GameObject prefabPixel;
    public GameObject parentObject;
    public int totalPixel;
    public GameObject GPUInstance;

    private void OnEnable()
    {
        instance = this;
        var currentLevel = PlayerPrefs.GetInt("currentLevel");
        Controller.colorCode = Random.Range(0,3);
        var crayon = parentObject.transform.GetChild(0).transform;
        totalPixel = 500;
        for (int i = 0; i < totalPixel; i++)
        {
            var color = Color.red;
            var spawnPixel = Instantiate(prefabPixel, parentObject.transform.position, Quaternion.identity);
            spawnPixel.transform.parent = crayon;
            spawnPixel.GetComponent<Renderer>().material.color = color;
            spawnPixel.GetComponent<Ball>().ballColor = color;
            spawnPixel.transform.localScale = Vector3.one * 0.02f;
        }
        GPUInstance.SetActive(true);
    }
}